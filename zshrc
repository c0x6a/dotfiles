if [[ $(ps --no-header -p $PPID -o comm) =~ '^yakuake|konsole$' ]]; then
    for wid in $(xdotool search --pid $PPID); do
        xprop -f _KDE_NET_WM_BLUR_BEHIND_REGION 32c -set _KDE_NET_WM_BLUR_BEHIND_REGION 0 -id $wid;
    done
fi
TERM="xterm-256color"
export ZSH=$HOME/.oh-my-zsh
export WORKON_HOME=~/virtenvs
ZSH_THEME="bullet-train"
BULLETTRAIN_EXEC_TIME_ELAPSED=2
BULLETTRAIN_PROMPT_ORDER=(
  time
  status
  dir
  virtualenv
  git
  cmd_exec_time
)
BULLETTRAIN_GIT_PROMPT_CMD=\${\$(git_prompt_info)//\\//\ \ }
HYPHEN_INSENSITIVE="true"
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"
HIST_STAMPS="dd/mm/yyyy"
BULLETTRAIN_CONTEXT_SHOW=true
BULLETTRAIN_VIRTUALENV_BG="green"
BULLETTRAIN_GIT_COLORIZE_DIRTY=true

plugins=(
    history-substring-search
    httpie
    git
    archlinux
    python
    pip
    django
    cp
    dirhistory
    compleat
    gpg-agent
    lol
    ssh-agent
    sudo
    systemd
    web-search
    wd
    zsh-navigation-tools
    adb
    virtualenv
    virtualenvwrapper
    colored-man-pages
    colorize
    copydir
    copyfile
    extract
    catimg
    zsh_reload
    z
)
source $ZSH/oh-my-zsh.sh

# User configuration
setopt NO_BEEP
export LANG="en_US.UTF-8"
export LANGUAGE="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export EDITOR='nvim'
export SSH_KEY_PATH="~/.ssh/rsa_id"
export PATH="$PATH:/home/cjdp/npm-global/bin:/opt/sonar-scanner/bin:$HOME/.npm/bin"
export PYENV_ROOT="$HOME/apps/pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/bin/virtualenvwrapper.sh

alias sshme="ssh cj@go.carlosjoel.net -p 13579"
alias sshdev="ssh usr_linux@10.10.3.121"
alias rebdev="gcd && ggl && gco - && grbd"
alias rebdevp='rebdev && ggf'
alias rebmast='gcm && ggl && gco - && grbm'
alias rebmastp='rebmast && ggf'
alias commitmsg='curl -s whatthecommit.com/index.txt'
alias prospector-django='prospector --uses django --strictness medium --max-line-length 120 -F -M'
alias ccat='pygmentize -g'
alias pokemonsay='$HOME/.pokemonsay/pokemonsay.sh'
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
eval "$(pyenv virtualenv-init -)"
alias lg='lazygit'
commitmsg | pokemonsay

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
###-begin-pm2-completion-###
### credits to npm for the completion file model
#
# Installation: pm2 completion >> ~/.bashrc  (or ~/.zshrc)
#

COMP_WORDBREAKS=${COMP_WORDBREAKS/=/}
COMP_WORDBREAKS=${COMP_WORDBREAKS/@/}
export COMP_WORDBREAKS

if type complete &>/dev/null; then
  _pm2_completion () {
    local si="$IFS"
    IFS=$'\n' COMPREPLY=($(COMP_CWORD="$COMP_CWORD" \
                           COMP_LINE="$COMP_LINE" \
                           COMP_POINT="$COMP_POINT" \
                           pm2 completion -- "${COMP_WORDS[@]}" \
                           2>/dev/null)) || return $?
    IFS="$si"
  }
  complete -o default -F _pm2_completion pm2
elif type compctl &>/dev/null; then
  _pm2_completion () {
    local cword line point words si
    read -Ac words
    read -cn cword
    let cword-=1
    read -l line
    read -ln point
    si="$IFS"
    IFS=$'\n' reply=($(COMP_CWORD="$cword" \
                       COMP_LINE="$line" \
                       COMP_POINT="$point" \
                       pm2 completion -- "${words[@]}" \
                       2>/dev/null)) || return $?
    IFS="$si"
  }
  compctl -K _pm2_completion + -f + pm2
fi
###-end-pm2-completion-###
