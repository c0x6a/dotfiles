# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

if [[ $(ps --no-header -p $PPID -o comm) =~ '^yakuake|konsole$' ]]; then
    for wid in $(xdotool search --pid $PPID); do
        xprop -f _KDE_NET_WM_BLUR_BEHIND_REGION 32c -set _KDE_NET_WM_BLUR_BEHIND_REGION 0 -id $wid;
    done
fi

# Attach to the tmux session on login via SSH.
if [ ! -z "$SSH_CLIENT" ] && [ -z "$DESKTOP_SESSION" -a -z "$TMUX" ] ; then
    tmux attach -t $HOST || tmux new -s $HOST
fi

TERM="xterm-256color"
export ZSH=$HOME/.oh-my-zsh
export WORKON_HOME=~/virtenvs

## ZSH Theme
ZSH_THEME="powerlevel10k/powerlevel10k"
###

plugins=(
    history-substring-search
    httpie
    git
    archlinux
    python
    pip
    django
    pyenv
    cp
    dirhistory
    compleat
    gpg-agent
    lol
    ssh-agent
    sudo
    systemd
    web-search
    wd
    zsh-navigation-tools
    adb
    virtualenv
#    virtualenvwrapper
    colored-man-pages
    colorize
    copydir
    copyfile
    extract
    catimg
    zsh_reload
    z
)

source $ZSH/oh-my-zsh.sh

# User configuration
setopt NO_BEEP
export LANG="en_US.UTF-8"
export LANGUAGE="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export EDITOR='vim'
export SSH_KEY_PATH="~/.ssh/rsa_id"
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
export PATH="$PATH:$(ruby -e 'puts Gem.user_dir')/bin"
export PATH="$PATH:$HOME/.npm/bin"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$PATH:$HOME/apps/flutter/bin"
export SMOKEUR_SERVER="https://u.c0x6a.dev"
export SMOKEUR_API_TOKEN="cotkMQH-n2_av011RW4ZUuhErJrX_M1gpvpaHy2FBfDW-3XhdbpzSKeumfl4S6rd2y4scpL7hZD7d0nV3Bv92w"
export BETTER_EXCEPTIONS=1
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/bin/virtualenvwrapper.sh

alias sshme="ssh go.carlosjoel.net"
alias commitmsg='curl -s whatthecommit.com/index.txt'
alias ccat='pygmentize -g'
alias pokemonsay='$HOME/.pokemonsay/pokemonsay.sh'
alias lg='lazygit'
alias ls='lsd'
alias py='python'

if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
eval "$(pyenv virtualenv-init -)"

#commitmsg | pokemonsay

# fzf integration

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/usr/share/fzf/completion.zsh" 2> /dev/null

# Key bindings
# ------------
source "/usr/share/fzf/key-bindings.zsh"


###-begin-pm2-completion-###
### credits to npm for the completion file model
#
# Installation: pm2 completion >> ~/.bashrc  (or ~/.zshrc)
#

COMP_WORDBREAKS=${COMP_WORDBREAKS/=/}
COMP_WORDBREAKS=${COMP_WORDBREAKS/@/}
export COMP_WORDBREAKS

if type complete &>/dev/null; then
  _pm2_completion () {
    local si="$IFS"
    IFS=$'\n' COMPREPLY=($(COMP_CWORD="$COMP_CWORD" \
                           COMP_LINE="$COMP_LINE" \
                           COMP_POINT="$COMP_POINT" \
                           pm2 completion -- "${COMP_WORDS[@]}" \
                           2>/dev/null)) || return $?
    IFS="$si"
  }
  complete -o default -F _pm2_completion pm2
elif type compctl &>/dev/null; then
  _pm2_completion () {
    local cword line point words si
    read -Ac words
    read -cn cword
    let cword-=1
    read -l line
    read -ln point
    si="$IFS"
    IFS=$'\n' reply=($(COMP_CWORD="$cword" \
                       COMP_LINE="$line" \
                       COMP_POINT="$point" \
                       pm2 completion -- "${words[@]}" \
                       2>/dev/null)) || return $?
    IFS="$si"
  }
  compctl -K _pm2_completion + -f + pm2
fi
###-end-pm2-completion-###

# Configurations only for ssh client
if [ ! -z "$SSH_CLIENT" ] ; then
    POWERLEVEL9K_OS_ICON_BACKGROUND='red'
    sc-restart nginx
    upgrade() {
        sudo pacman -Syyu --noconfirm
        upgrade_oh_my_zsh
    }
else
    upgrade() {
        echo "Upgrading Arch base system...\n"
	sudo pacman -Syyu --noconfirm
        sudo pikaur -Syua --noconfirm
        echo "\n\nUpgrading flatpak packages...\n"
        flatpak update -y
	echo "\n\nUpgrading pyenv"
	cd ~/.pyenv;git pull;cd ~
	echo "\n\nUpgrading PowerLevel10k"
	cd ~/.oh-my-zsh/custom/themes/powerlevel10k;git pull;cd ~
        echo "\n\nUpgrading Oh-My-ZSH...\n"
        omz update
    }
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

