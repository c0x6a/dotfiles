set background=dark
set clipboard=unnamed

set nocompatible
set laststatus=2
set encoding=utf-8
set t_Co=256
filetype off

" git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
" vim +BundleInstall +qall

set rtp+=~/.vim/bundle/vundle/
set rtp+=~/.vim/bundle/powerline/powerline/bindings/vim
call vundle#rc()

Bundle 'gmarik/vundle'
Bundle 'nvie/vim-flake8'
Bundle 'tpope/vim-fugitive'
Bundle 'Lokaltog/powerline'
Bundle 'msanders/cocoa.vim'
Bundle 'msanders/snipmate.vim'
Bundle 'Lokaltog/vim-easymotion'

filetype plugin indent on

setlocal comments-=:#
setlocal comments+=f:#
syntax on
set mouse=a
set bg=dark tabstop=4 softtabstop=4 shiftwidth=4 expandtab
set hlsearch
au BufRead,BufNewFile *.py,*.pyw set colorcolumn=80
au BufRead,BufNewFile *.md set colorcolumn=73
au BufRead,BufNewFile *.html,*.js,*.css set colorcolumn=120
set smartindent
set copyindent
set incsearch
set wildignore=*.swp,*.bak,*.pyc,*.class
set autoindent
set noswapfile
set fileencodings=utf-8,ucs-bom,default,latin1
set cursorline
set cursorcolumn
set showcmd
set hlsearch
set incsearch
set backspace=indent,eol,start
set ruler
set number
set scrolloff=5 "provide some context when scrolling
set showmatch
set wildchar=<Tab>
set wildmenu
set number relativenumber
"set nonumber norelativenumber  " turn hybrid line numbers off

highlight SpecialKey ctermbg=yellow ctermfg=black guibg=yellow guifg=black
highlight ColorColumn ctermbg=Grey guibg=Grey
highlight LineNr ctermfg=DarkYellow
set list
set listchars=trail:.,tab:>-,nbsp:%,extends:>,precedes:<

"filetype plugin indent on

autocmd FileType python set omnifunc=pythoncomplete#Complete

let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1

"nnoremap <F5> :GundoToggle<CR>

" Some Magic
if &term =~ "xterm"
    let &t_SI = "\<Esc>]12;orange\x7"
    let &t_EI = "\<Esc>]12;white\x7"
endif
set statusline=%F%m%r%h%w\ %y\ [row=%l/%L]\ [col=%02v]\ [%02p%%]\

